#include "ServerCommunication.h"

ServerCommunication::~ServerCommunication() {
	closesocket(sock);
}

int Server::Init() {
	WORD ver = MAKEWORD(2, 2);

	int wsOk = WSAStartup(ver, &wsData);
	if (wsOk != 0)
	{
		std::cerr << "Can't Initialize winsock! Quitting" << std::endl;
		return -1;
	}
	int res;
	sockInfo.sin_family = AF_INET;
	sockInfo.sin_port = htons(port);
	sockInfo.sin_addr.s_addr = INADDR_ANY;

	memcpy(&sockAddress, &sockInfo, sizeof(SOCKADDR_IN));

	res = WSAStartup(MAKEWORD(1, 1), &wsData);
	std::cout << "\n\nWSAStartup"
		<< "\n Version: " << wsData.wVersion
		<< "\n Description: " << wsData.szDescription
		<< "\n Status: " << wsData.szSystemStatus << std::endl;

	if (res != 0)
		s_cl("WSAStarup failed", WSAGetLastError());
	return(res);
}

void ServerCommunication::s_handle(int s)
{
	if (sock)
		closesocket(sock);
	WSACleanup();
	exit(0);
}

void ServerCommunication::s_cl(const char* a, int x)
{
	std::cout << a;
	s_handle(x + 1000);
}

Server::Server() {
	this->clientCount = 0;
	this->Init();
	this->createSocket();
}

Server::~Server() {
	endServer();
}

void Server::createSocket() {
	int res, i = 1;

	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sock == INVALID_SOCKET)
		s_cl("Invalid Socket ", WSAGetLastError());
	else if (sock == SOCKET_ERROR)
		s_cl("Socket Error)", WSAGetLastError());
	else
		std::cout << "SOCKET Established" << std::endl;

	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*)&i, sizeof(i));

	res = bind(sock, &sockAddress, sizeof(sockAddress));
	std::cout << "Binding socket:" << res << std::endl;
	if (res != 0)
		s_cl("BIND FAILED", WSAGetLastError());
	else
		std::cout << "Socket Bound to port : " << port << std::endl;


	res = listen(sock, 5);
	std::cout << "Server Listen mode : " << res << /*" Size = " << m * 2 <<*/ std::endl;

	unsigned long b = 1;
	ioctlsocket(sock, FIONBIO, &b);
}

int Server::acceptClient(_client* x) {
	x->i = sizeof(sockaddr);
	x->cs = accept(sock, (sockaddr*)&x->addr, &x->i);
	if (x->cs != 0 && x->cs != SOCKET_ERROR)
	{
		x->con = true;
		FD_ZERO(&x->set);
		FD_SET(x->cs, &x->set);
		return (true);
	}
	return (false);
}

int Server::sendToClient(_client* x, const char* buffer, int sz)
{
	x->i = send(x->cs, buffer, sz, 0);
	if (x->i == SOCKET_ERROR || x->i == 0)
	{
		disconnect_client(x);
		return (false);
	}
	else return (true);
}

int Server::recvFromClient(_client* x, char* buffer, int sz)
{
	if (FD_ISSET(x->cs, &x->set))
	{
		x->i = recv(x->cs, buffer, sz, 0);
		if (x->i == 0)
		{
			disconnect_client(x);
			return (false);
		}
		return (true);
	}

	return (false);
}

void Server::acceptClients()
{
	for (int i = 0; i < MAX_CLIENTS; i++)
	{
		if (!client[i].con)		//i.e a client has not connected to this slot
		{
			if (acceptClient(&client[i]))
			{
				//TODO send info about user joining
			}
		}
	}
}

void Server::chat_message(const char* s)
{
	int len = strlen(s);

	for (int i = 0; i < MAX_CLIENTS; i++)
	{
		if (client[i].con)		//valid slot,i.e a client has parked here
		{
			sendToClient(&client[i], s, len);
		}
	}
}

void Server::recvClients()
{

	for (int i = 0; i < MAX_CLIENTS; i++)
	{
		if (client[i].con)		//valid slot,i.e a client has parked here
		{
			char buffer[BF_SZ];;
			if (recvFromClient(&client[i], buffer, BF_SZ))
			{
				chat_message(buffer);
			}
		}
	}
}

void Server::sendClients(const char* buffer, int sz)
{

	for (int i = 0; i < MAX_CLIENTS; i++)
	{
		if (client[i].con)		//valid slot,i.e a client has parked here
		{
			if (sendToClient(&client[i], buffer, BF_SZ))
			{

			}
		}
	}
}

int Server::disconnect_client(_client* current_client)
{ // Disconnect a client
	if (current_client->con == TRUE)
	{ // Close the socket for the client
		closesocket(current_client->cs);
	}
	// Set the new client state
	current_client->con = FALSE;
	// Decrement the current number of connected clients
	clientCount--;

	std::cout << "Disconnecting client[]" << std::endl;
	return (1);
}

void Server::endServer() {
	for (int j = 0; j < MAX_CLIENTS, j++;) { disconnect_client(&client[j]); }
	// Close the listening socket for the server
	closesocket(sock);
	// Clean up winsock
	WSACleanup();
}

Client::Client() {
	this->Init();
	this->createSocket();
}

Client::~Client() {

}

int Client::Init() {
	int res;

	sockInfo.sin_family = AF_INET;
	sockInfo.sin_port = htons(port);					//Set the port
	//sockInfo.sin_addr.s_addr = inet_addr(IP.c_str());		//Set the address we want to connect to
	if (!inet_pton(AF_INET, IP.c_str(), &sockInfo.sin_addr.s_addr)) {
		std::cout << "ERRROR PTON\n" << IP.c_str();
	}
	memcpy(&sockAddress, &sockInfo, sizeof(SOCKADDR_IN));

	res = WSAStartup(MAKEWORD(1, 1), &wsData);		//Start Winsock
	std::cout << "\n\nWSAStartup"
		<< "\nVersion: " << wsData.wVersion
		<< "\nDescription: " << wsData.szDescription
		<< "\nStatus: " << wsData.szSystemStatus << std::endl;
	if (res != 0)
		s_cl("WSAStarup failed", WSAGetLastError());
	return(res);
}


void Client::createSocket() {
	int res;
	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);		//Create the socket
	if (sock == INVALID_SOCKET)
		s_cl("Invalid Socket ", WSAGetLastError());
	else if (sock == SOCKET_ERROR)
		s_cl("Socket Error)", WSAGetLastError());
	else
		std::cout << "Socket Established" << std::endl;

	res = connect(sock, &sockAddress, sizeof(sockAddress));				//Connect to the server
	if (res != 0)
	{
		s_cl("SERVER UNAVAILABLE", res);
	}
	else
	{
		std::cout << "\nConnected to Server: ";
		memcpy(&sockInfo, &sockAddress, sizeof(SOCKADDR));
	}
}