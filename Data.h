#pragma once
#include <string>
#include <sstream>
#include <vector>
#include "sha256.h"
#include "Text.h"

class Data: public Text {
public:
	Data();
	Data(std::string text);
	std::string getType();
};

class User : public Data {
public:
	User(std::string nick, std::string password);
private:
};

class Message : public Data {
public:
	Message(std::string nick, std::string message);
private:
};