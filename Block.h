//
// Created by Dave Nash on 20/10/2017.
//

#ifndef TESTCHAIN_BLOCK_H
#define TESTCHAIN_BLOCK_H

#include <cstdint>
#include <iostream>
#include <time.h>
#include "Data.h"

using namespace std;

class Block {
public:
    string sHash;
    string sPrevHash;

	int getIndex();
	string getBlockData();
	Data* getBlockNode();

    Block(Data* dataNode);
	Block(string blockData);
	Block(int index, string hash, string prevHash, int nonce, int timestamp, string data);

    void MineBlock(uint32_t nDifficulty);
	bool validate(string prevHash);
	void discard();

private:
    uint32_t _nIndex;
    uint32_t _nNonce;
    Data* _sData;
    time_t _tTime;

    string _CalculateHash() const;
	static int blockCount;
};

#endif //TESTCHAIN_BLOCK_H
