#include <WS2tcpip.h>
#include <winsock.h>
#include <sstream>
#include <string>
#include <iostream>

#pragma comment(lib, "Ws2_32.lib")
using namespace std;
#define MAX_CLIENTS 10
#define BF_SZ 1024
const int port = 54000;
const std::string IP = "192.168.0.135";

struct _client
{
	bool con = false;			//Set true if a client is connected
	sockaddr_in addr;	//Client info like ip address
	SOCKET cs;		//Client socket
	fd_set set;			//used to check if there is data in the 
	string nick;
	string publicKey;
	int i;
};

class ServerCommunication {
public:
	~ServerCommunication();
	virtual int Init() = 0;
	void s_cl(const char* a, int x);
	void s_handle(int s);
	SOCKET sock;

protected:

	WSADATA wsData;
	sockaddr_in sockInfo;
	sockaddr sockAddress;

private:
	virtual void createSocket() = 0;

};

class Server : public ServerCommunication {
public:

	Server();
	~Server();
	int Init();
	void acceptClients();
	void sendClients(const char* buffer, int sz);
	virtual void recvClients();
	int sendToClient(_client* x, const char* buffer, int sz);
	void endServer();
	void chat_message(const char* s);

protected:

	int acceptClient(_client* x);
	int recvFromClient(_client* x, char* buffer, int sz);
	int disconnect_client(_client* current_client);

	void createSocket();
	struct _client client[MAX_CLIENTS];
	int clientCount;

};

class Client : public ServerCommunication {
public:
	Client();
	~Client();
	int Init();

private:
	void createSocket();
	//SOCKET servSock;

};