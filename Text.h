#pragma once
#include <string>
#include <sstream>
#include <vector>

class Text {
public:
	Text();
	Text(std::string);
	~Text();
	std::string* operator [](const int index);
	std::string get(const int index);
	std::string get(const int first, const int last);
	std::string whole();
	std::string formatted(int index);
	int size();
protected:
	std::vector<std::string> elements;
};