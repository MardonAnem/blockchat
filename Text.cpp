#include "Text.h"

Text::Text() {

}

Text::Text(std::string text) {
	size_t end;
	while (!text.empty()) {
		if ((text.compare(0, 2, "/'") == 0) && (end = text.find("/'", 2)) != std::string::npos) {
			this->elements.push_back(text.substr(0, end + 2));
			text.erase(0, end + 3);
		}
		else if ((end = text.find(' ')) != std::string::npos) {
			this->elements.push_back(text.substr(0, end));
			text.erase(0, end + 1);
		}
		else {
			this->elements.push_back(text);
			text.clear();
		}
	}
}

Text::~Text() {
	this->elements.clear();
}

std::string* Text::operator [](const int index) {
	return &elements[index];
}

std::string Text::get(const int index) {
	return elements[index];
}

std::string Text::get(const int first, const int last) {
	int realLast = last;
	if (last > elements.size()) {
		realLast = elements.size();
	}
	std::string result;
	int i = first;
	for (i; i < realLast-1; i++) {
		result = result + this->get(i) + ' ';
	}
	result = result + this->get(i);
	return result;
}

std::string Text::formatted(int index) {
	if ((elements[index].compare(0, 2, "/'") == 0) && (elements[index].compare(elements[index].length() - 2, 2, "/'") == 0)) {
		return (elements[index].substr(2, elements[index].length() - 4));
	}
	return elements[index];
}

std::string Text::whole() {
	std::string whole;
	int i = 0;
	for (i; i < this->size() - 1; i++) {
		whole = whole + elements[i] + " ";
	}
	whole += elements[i];
	return whole;
}

int Text::size() {
	return this->elements.size();
}