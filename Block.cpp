//
// Created by Dave Nash on 20/10/2017.
//

#include "Block.h"
#include "sha256.h"

int Block::blockCount = 0;

Block::Block(string blockData) {
	Text text(blockData);
	this->_nIndex = atoi(text[0]->c_str());
	if ((int)this->_nIndex > (blockCount - 1)) {
		blockCount = this->_nIndex + 1;
	}
	this->sHash = text[1]->c_str();
	this->sPrevHash = text[2]->c_str();
	this->_nNonce = atoi(text[3]->c_str());
	this->_tTime = atoi(text[4]->c_str());
	this->_sData = new Data(*(text[5]) + ' ' + *(text[6]) + ' ' + *(text[7]));
}

Block::Block(Data* dataNode) : _nIndex(blockCount++), _sData(dataNode)
{
    _nNonce = 0;
    _tTime = time(nullptr);
    sHash = _CalculateHash();
}

void Block::discard() {
	blockCount--;
}

void Block::MineBlock(uint32_t nDifficulty)
{

	char* cstr = new char[nDifficulty + 1];
    for (uint32_t i = 0; i < nDifficulty; ++i)
    {
        cstr[i] = '0';
    }
    cstr[nDifficulty] = '\0';

    string str(cstr);

    do
    {
        _nNonce++;
        sHash = _CalculateHash();
    }
    while (sHash.substr(0, nDifficulty) != str);

    cout << "Block mined: " << sHash << endl;
}

bool Block::validate(string prevHash) {
	return this->sHash == _CalculateHash() && this->sPrevHash == prevHash;
}

inline string Block::_CalculateHash() const
{
    stringstream ss;
    ss << _nIndex << sPrevHash << _tTime << _sData->whole() << _nNonce;

    return sha256(ss.str());
}

Block::Block(int index, string hash, string prevHash, int nonce, int timestamp, string data) {
	if ((this->_nIndex = index) + 1 > blockCount) {
		blockCount = index + 1;
	}
	this->sHash = hash;
	this->sPrevHash = prevHash;
	this->_nNonce = nonce;
	this->_sData = new Data(data);
	this->_tTime = timestamp;
}

string Block::getBlockData() {
	stringstream blocktxt;
	blocktxt << _nIndex << " " << sHash << " " << sPrevHash << " " << _nNonce << " " << _tTime << " " << _sData->whole();
	return blocktxt.str();
}

Data* Block::getBlockNode() {
	return this->_sData;
}
int Block::getIndex() {
	return this->_nIndex;
}