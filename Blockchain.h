//
// Created by Dave Nash on 20/10/2017.
//

#ifndef TESTCHAIN_BLOCKCHAIN_H
#define TESTCHAIN_BLOCKCHAIN_H

#define USER_KEY 1
#define USER_NICK 2

#include <cstdlib>
#include "Block.h"
#include <fstream>

using namespace std;

class Blockchain {
public:
    Blockchain();
	~Blockchain();

    void AddBlock(Block bNew);
	int searchUser(Data* user);
	int searchUser(string data, int mode = USER_KEY);
	void showMessages();

	void load();
	void load(unsigned int index);
	int save();

protected:
    uint32_t _nDifficulty;
    vector<Block> _vChain;
	fstream file;
    Block _GetLastBlock() const;
};

#endif //TESTCHAIN_BLOCKCHAIN_H
