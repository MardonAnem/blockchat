//
// Created by Dave Nash on 20/10/2017.
//

#include "Blockchain.h"

Blockchain::Blockchain()
{
	file.open("blockchain.txt", fstream::out | fstream::in);
	if (!file.is_open()) {
		file.open("blockchain.txt", fstream::out | fstream::in | fstream::trunc);
		Block genesis(new User("Admin", "admin"));
		genesis.sPrevHash = "-1";
		genesis.MineBlock(_nDifficulty);
		_vChain.emplace_back(genesis);
	}
	else {
		this->load();
	}
    _nDifficulty = 0;
}

Blockchain::~Blockchain() {
	_vChain.clear();
}

int Blockchain::searchUser(Data* user) {
	for (int i = 0; i < this->_vChain.size(); i++) {
		if ((_vChain[i].getBlockNode()->getType() == "User") && (_vChain[i].getBlockNode()->whole() == user->whole())) {
			return _vChain[i].getIndex();
		}
	}
	return -1;
}

int Blockchain::searchUser(string data, int mode) {
	for (int i = 0; i < this->_vChain.size(); i++) {
		if (_vChain[i].getBlockNode()->getType() == "User" && data == _vChain[i].getBlockNode()->get(mode)) {
			return _vChain[i].getIndex();
		}
	}
	return -1;
}

void Blockchain::showMessages() {
	for (int i = 0; i < _vChain.size(); i++) {
		if (_vChain[i].getBlockNode()->getType() == "Message") {
			cout << _vChain[i].getBlockNode()->get(1) << ": " << _vChain[i].getBlockNode()->formatted(2) << endl;
		}
	}
}

int Blockchain::save() { 
	file.clear();
	file.seekp(0, ios::beg);
	file.seekg(0, ios::beg);
	int test1 = file.tellp();
	int test2 = file.tellg();
	int startBlock = _vChain[0].getIndex();
	for (int i = 0; i < startBlock; i++) {
		string tmp;
		getline(file, tmp);
	}
	file.clear();

	for (int i = 0; i < _vChain.size(); i++) {
		if (i != 0) file << endl;
		file << _vChain[i].getBlockData();
	}
	return 0;
}

void Blockchain::load() {
	file.seekg(0, ios::beg);
	int blockCount = 0;
	string blockData;
	while (getline(file, blockData)) {
		Block bNew(blockData);
		string prevHashVal;
		if (blockCount == 0) {
			prevHashVal = "-1";
		}
		else {
			prevHashVal = _GetLastBlock().sHash;
		}
		if (bNew.validate(prevHashVal)) {
			blockCount++;
			_vChain.push_back(bNew);
		}
		else {
			cout << "Error at block " << bNew.getIndex() << endl;
			bNew.discard();
		}
	}
}

void Blockchain::load(unsigned int index) {
	if (index == 0) { //load only last block
		file.seekg(-1, ios_base::end);
		bool flag = true;
		while (flag) {
			char tmp;
			file.get(tmp);
			if ((int)file.tellg() <= 1) {
				file.seekg(0, ios::beg);
				flag = false;
			}
			else if (tmp == '\n') {
				flag = false;
			}
			else {
				file.seekg(-2, ios_base::cur);
			}
		}
	}
	else { //load every block starting from index
		file.seekg(0, ios_base::beg);
		for (int i = 0; i < index; i++) {
			string tmp;
			if (!getline(file, tmp)) {
				file.clear();
				break;
			}
		}
	}

	string blockData;
	bool flag = false;
	while (getline(file, blockData)) {
		
		Block bNew(blockData);
		string prevHashVal;
		if (flag) {
			prevHashVal = _GetLastBlock().sHash;

			if (bNew.validate(prevHashVal)) {
				_vChain.push_back(bNew);
			}
			else {
				cout << "Error at block " << bNew.getIndex() << endl;
				bNew.discard();
			}
		}
		else {
			_vChain.push_back(bNew);
			flag = true;
		}
	}
}

void Blockchain::AddBlock(Block bNew)
{
    bNew.sPrevHash = _GetLastBlock().sHash;
    bNew.MineBlock(_nDifficulty);
    _vChain.push_back(bNew);
}

Block Blockchain::_GetLastBlock() const
{
	return _vChain.back();
}
